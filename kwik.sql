select
v.*,
CASE WHEN
v.ENTITY_ID = fraud.ENTITY_ID THEN 1 ELSE 0 END AS FraudFlag
from V_DEALER_INFO v
left join
(select * from V_DEALER_INFO
WHERE ENTITY_ID IN
('lincolnfurn',
'chitownfurnlinen',
'discountfurniture',
'valuezonefandm',
'buybetter',
'homelinen',
'goldenmatt',
'queencityfurn')) AS fraud
on fraud.ENTITY_ID = v.ENTITY_ID